\capitulo{Determinación y justificación del framework}

A continuación se describe el patrón de diseño Modelo-Vista-Controlador (MVC) y
algunos frameworks \footnote{Es una abstracción en la que el software provee
funciones genéricas que pueden ser selectivamente cambiadas por código escrito
por el usuario.} que utilizan el MVC para su arquitectura interna. Finalmente se
realiza una comparación entre todos los frameworks expuestos y se justificará el
seleccionado para el desarrollo de este proyecto.

Entre los frameworks a comparar se encuentran: Django (Python), Ruby On Rails
(Ruby) y Yesod (Haskell). Se escogieron estos a comparar debido a que son
populares y por lo menos un miembro del equipo conoce algo de ellos, a excepción
de Ruby on Rails.


\seccion{Modelo-Vista-Controlador}

Es un patrón de arquitectura de software que separa los datos (modelo), la
lógica (controlador) y la interfaz del usuario (vista). Se basa en la idea de la
reutilización de código y separación de conceptos, características que buscan
facilitar la tarea de desarrollo de aplicaciones y posterior mantenimiento
(\cite{Wiki:MVC}).

La separación de conceptos es un principio de diseño para separar un programa de
computadora en distintas secciones tal que cada una de ellas es responsable de
un conjunto en particular de información (\cite{Wiki:SeparationOfConcerts})
(\cite{Wiki:Concern}).


\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.7]{imagenes/MVC-Process.png}
	\caption{Descripción gráfica del Modelo-Vista-Controlador (\cite{ImagenMVC})}
\end{figure}

El modelo es la representación de la información con la cual el sistema opera.
Gestiona los accesos a la información (CRUD: Create, Read, Update y Delete).
Envía a la vista la información que se le solicita.


El controlador responde a los eventos (acciones del usuario) e invoca peticiones
al modelo cuando se hace alguna solicitud sobre la información.

En cuanto a la vista, esta presenta al modelo en un formato adecuado para
que el usuario pueda interactuar con la información.



\newpage
\seccion{Django}


Según la documentación de Django (\cite{DjangoInicial}):

\begin{quote}
    Django es un framework web de alto nivel en Python que alienta el desarrollo
    rápido y limpio.
\end{quote}

Fue diseñado para hacer tareas comunes en el desarrollo web, rápidas y fáciles
(\cite{Django:Intro}). Se basa en Modelo-Vista-Controlador.


Se puede describir completamente una base de datos por medio de modelos en
Django. Estos modelos son clases con atributos siendo cada clase una tabla en la
base de datos y cada atributo una columna. Estos modelos son agnósticos a la
base de datos pudiendo cambiar de base de datos de manera sencilla siempre y
cuando no se utilicen sentencias SQL escritas a mano o no se utilicen
características propias de un motor de base de datos (\cite{Django:DB}).

Dentro de las bases de datos soportadas oficialmente por Django 1.6 (versión
estable al momento de escribir este informe) se encuentran: PostgresSQL (8.4 o
superior), MySQL (5.0.3), SQLite y Oracle.

PostgresSQL es la base de datos recomendada para usar con Django debido a que
los desarrolladores de Django prefieren esa base de datos
(\cite{Django:Prerequisitos}).

El controlador en Django es escrito en Python. Estos piden al modelo datos,
trabajan con ellos y después se los pasan a la vista.


Las vistas son HTML. Django provee de un método llamado templates que permite
tener código mezclado con el HTML. Este lenguaje permite hacer operaciones
simples como decidir si mostrar ciertas cosas (ifs) o iterar a través de
colecciones de datos (con fors).


Django viene con una interfaz dinámica desde la cual se puede realizar
operaciones CRUD sobre las tablas de la base de datos.


Otro punto a considerar es el lenguaje en el cual esta desarrollado Django.
Python es un lenguaje de programación de tipado dinámico e interpretado
orientado a objetos (\cite{Python:QueEs}).

Python tiene bastantes librerías disponibles para realizar todo tipo de
operaciones, desde librerías científicas hasta de creación de tareas con Celery.



\seccion{Yesod}

Según la documentación de Yesod (\cite{Yesod:QueEs}):

\begin{quote}
    Yesod es un framework web para el lenguaje de programación Haskell para el
    desarrollo de aplicaciones de tipado seguro, RESTful y de alto performance.
\end{quote}

Yesod pretende transformar los errores en tiempo de ejecución (comunes en los
lenguajes interpretados y de tipado dinámico) en errores en tiempo de
compilación. Yesod es asíncrono por defecto debido a que Haskell es asíncrono.
Haskell utiliza hilos verdes \footnote{Los green threads son hilos que son
planificados por una máquina virtual (o runtime) en vez del sistema operativo.}
y llamadas al sistema basadas en eventos lo que hace que el código escrito en
este lenguaje sea automáticamente no bloqueante.

Yesod además provee de varios DSL \footnote{Domain-specific language. Son
lenguajes especializados para ser aplicados en un dominio específico. También
son llamados mini-lenguajes} para dar una sintaxis liviana a tareas comunes en
los frameworks webs como la definición de modelos o rutas. Al momento de
compilar la aplicación, estos lenguajes son traducidos a código Haskell de
manera automática para que su tipado sea verificado.


Yesod también es Modelo-Vista-Controlador. Los modelos son creados utilizando un
lenguaje simple dado por una librería llamada ``Persistent'', que luego es
traducido en tipos nativos de Haskell. Cada uno de estos modelos permite hacer
operaciones CRUD y modelar las relaciones entre cada tabla. El controlador es
una función en Haskell que es llamada si es que se encuentra una ocurrencia en
el archivo de rutas el cual está escrito en un lenguaje simple. Debido a que
Haskell es de tipado estático, la función no debe preocuparse por errores en los
argumentos ya que se garantiza que si la función es llamada, entonces el dato
pasado es del tipo esperado. En cuanto a las vistas, Haskell provee un sistema
de templates llamado Hamlet que permite iterar en una colección de datos o
realizar decisiones simples con ifs, escribir CSS por medio de un lenguaje
llamado Cassius y Javascript en uno llamado Julius (\cite{Yesod:Ideas}).

Los templates en Haskell también son compilados antes de ejecutar la aplicación
para comprobar que todos los datos utilizados corresponden a lo que se espera
que sea pasado por argumento.


En cuanto al lenguaje en el que esta hecho Yesod, Haskell es un lenguaje de
programación puramente funcional que lleva más de 20 años siendo utilizado en la
academia para investigar sobre los lenguajes de programación. Permite el
desarrollo rápido de aplicaciones robustas, concisas y correctas debido a su
avanzado sistema de tipado (\cite{Haskell:QueEs}).

Haskell contiene bastantes librerías relacionadas con el desarrollo de software.
Tal vez una de las más útiles es una llamada ``Esqueleto''. Esqueleto permite
utilizar un lenguaje parecido a SQL para realizar las consultas de una manera
segura desde el punto de vista del tipado y previene las inyecciones SQL
(\cite{Haskell:Esqueleto}).


Debido a la naturaleza del runtime de Haskell es sencillo escribir programas que
funcionen en paralelo. En la última versión del compilador de Haskell, la 7.8.1
liberada el 9 de abril de 2014, se incluye un nuevo gestor de entrada y salida
que permite escalar un servidor web escrito en este lenguaje hasta 40 núcleos
del procesador (\cite{Haskell:Mio}).


\seccion{Ruby on Rails}


Según la documentación de Ruby on Rails (\cite{Rails:QueEs}):

\begin{quote}
    Ruby on Rails es un framework web de código abierto que está optimizado para
    la felicidad del programador y productividad sustentable. Permite escribir
    código hermoso favoreciendo la convención sobre la documentación.
\end{quote}


Ruby on Rails es un framework MVC construido sobre el lenguaje Ruby. Permite el
rápido desarrollo de aplicaciones utilizando el concepto de convención sobre
configuración. Existe un evento anual que consiste en pequeños equipos que
desarrollan aplicaciones en Rails en 48 horas. Esto demuestra que desarrollar
aplicaciones en Rails es bastante rápido (\cite{Rails:Libro}).


Ruby es utilizado en varias páginas webs populares, una de ellas es GitHub.
GitHub es un servicio de repositorios de git lanzado en Febrero de 2008
(\cite{Rails:Libro}).


Tal vez una de las características más interesantes de Rails para el curso de
``Proyecto de Base de Datos'' es el scaffolding. Scaffolding es una técnica
soportada por algunos frameworks MVC en los que el programador escribe las
especificaciones que describen como la base de datos va a ser utilizada por la
aplicación. Luego el compilador genera código que la aplicación puede utilizar
para hacer operaciones CRUD en entradas de la base de datos
(\cite{Scaffolding}). Esta habilidad para crear código de forma automática hace
de Rails un framework que permite el desarrollo rápido de aplicaciones web.


En cuanto al lenguaje, Rails es un lenguaje de programación dinámico y de código
abierto enfocado a la simplicidad y productividad (\cite{Ruby:QueEs}).
Inicialmente, el creador de Ruby buscó en otros lenguajes para encontrar la
sintaxis ideal. La idea era crear un lenguaje ``que fuera más poderoso que Perl,
y más orientado a objetos que Python'' (\cite{Ruby:About}). En Ruby todo es un
objeto, incluso los tipos de datos primitivos.



\seccion{Comparación}


A continuación se hace una comparación de los frameworks descritos en las
secciones anteriores.


\tabla{Comparación de frameworks}{
\begin{longtabu} to \textwidth {|c|X[1, l]|X[1,l]|X[1, l]|}
        \hline
        Categoría & Rails & Yesod &  Django \\ \hline
        \endhead
        Sistema operativo &
            Linux, Mac OS X, Windows, QNX &
            Linux, Mac OS X, Windows &
            Linux, Mac OS X, Windows \\ \hline

        Lenguaje de programación &
            Ruby &
            Haskell &
            Python \\ \hline
        
        Interpretado (Lenguaje) &
            Sí &
            No, Compilado &
            Sí \\ \hline
        
        Tipado (Lenguaje) &
            Dinámico &
            Estático &
            Dinámico \\ \hline

        Velocidad del lenguaje \footnote{Comparado usando n-body y respecto al
            mejor lenguaje probado que es fortran(\cite{LenguajesComparacion})} &
            Ruby es 73 veces más lento que Fortran &
            Haskell es 2.6 veces más lento que Fortran &
            Python es 104 veces más lento que Fortran \\ \hline

        Compilado &
            No &
            Sí &
            No \\ \hline

        Scaffolding &
            Sí &
            No &
            Si, con la interfaz de administración \\ \hline

        RESTful &
            Sí &
            Sí &
            Sí \\ \hline

        Paradigma del lenguaje &
            Multiparadigma &
            Funcional &
            Multiparadigma \\ \hline

        Soporte en Heroku &
            Sí &
            Sí (Con librerías externas) &
            Sí \\ \hline

        Templates &
            Sí &
            Sí &
            Sí \\ \hline

        Soporte para formularios &
            Sí &
            Sí &
            Sí \\ \hline

        Soporte para PostgreSQL &
            Sí &
            Sí &
            Sí \\ \hline


\end{longtabu}}


Entonces es posible concluir que los 3 frameworks son bastante parecidos. Todos
son MVC por lo que su organización interna es bastante parecida.

Se elige como framework a utilizar Ruby on Rails principalmente por el
scaffolding, el cual permite crear aplicaciones web rápidamente. Con esta
elección se pretende disminuir el riesgo del tiempo que está presente en este
proyecto.


Se descarta Yesod ya que, si bien permite crear aplicaciones correctas desde el
punto de vista de las comprobaciones que realiza el compilador, Haskell es un
lenguaje difícil de aprender por lo que se perdería tiempo en avanzar hasta una
aplicación terminada. 


En cuanto a Django, se descarta simplemente por no tener el nivel de soporte
para scaffolding que tiene Ruby on Rails.
