# -*- coding: utf-8 -*-
from os import listdir
from os.path import isfile, join

mypath = "./modelos/mockups/imagenes/"

onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]


f = open("mapa_navegacion.tex", "w")

var = 1


for foto in onlyfiles:
    c = """

%% Imagen %d
\\newpage
\\tabla{Una tabla}{
\\begin{tabularx}{\\textwidth}{|X|}
        
    \hline
    Mockup %d \cellcolor[gray]{0.9} \\\\ \\hline
    
    \\includegraphics[scale=0.53]{modelos/mockups/imagenes/%s} \\\\ \\hline


    Privilegios: \\\\ \\hline
    Descripción: \\\\ \\hline

\\end{tabularx}
}
    """ %(var, var, foto)
    f.write(c)
    f.write("\n\n")
    var += 1

f.close()
